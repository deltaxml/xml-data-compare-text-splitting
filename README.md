# Text Splitting


For details about this sample see our [web pages.](https://docs.deltaxml.com/xdc/latest/samples/text-splitting)

Using the file config-empty.xml will give the default value of 'on' for the text-splitting feature for the whole comparison.  

Using config-ts-false.xml will process the comparison with text-splitting switched off.  

Finally using config-ts-specific will have text-splitting switched off except for the `extra` element.  


## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-ts-specific.xml</path>
    </configuration>
</compare>
```



See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.

